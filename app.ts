import {Request, Response} from "express";

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;
const { getFootprint } = require("./footprintCalculator");
const { getAllTransactions, saveNewTransactions } = require('./api/transactions')


app.use(bodyParser.text({ type: 'text/csv' }));

app.get('/footprint', async (req: Request, res: Response) => {
  const footprint = await getFootprint();
  res.json(footprint);
});

app.get('/transactions', getAllTransactions);


app.post('/transactions', saveNewTransactions);


app.get('/', (req: Request, res: Response) => res.json({ server_status: 'working' }))

const server = app.listen(port);

module.exports = server;
