'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addIndex("Transactions", {
      unique: true,
      name: 'uniqueTransactionsIndex',
      fields: [
        'userId',
        'transactionDate',
        'memo'
      ]
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeIndex('Transactions', 'uniqueTransactionsIndex');
  }
};
