'use strict';
const carbonWeights = require('../config/carbonWeights.json');

const VALID_CATEGORIES = ['food', 'home', 'goods_and_services', 'transport', 'electronic_services'];
const CATEGORY_KEYWORDS = {
  food: [
      'restaurants',
      'grocery',
      'food',
  ],
  home: [],
  'goods_and_services': [
      'clothes',
      'furniture',
      'general',
  ],
  transport: [
      'gasoline',
      'travel',
      'transportation',
  ],
  'electronic_services': [
      'financial',
      'online',
  ]
}
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    memo: {
      type: DataTypes.STRING,
      allowNull: false
    },
    carbonCategory: {
      type: DataTypes.ENUM,
      values: VALID_CATEGORIES,
      allowNull: false
    },
    transactionAmount: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    transactionDate: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {});
  Transaction.associate = function(models) {
    Transaction.belongsTo(models.User, {
      foreignKey: 'userId'
    });
  };


  Transaction.prototype.carbonAmount = function () {
    const carbonAmount = this.transactionAmount * carbonWeights[this.carbonCategory];
    return parseFloat(carbonAmount.toFixed(2))
  };

  Transaction.tryFindCarbonCategory = function (category) {
    for (let i = 0; i < VALID_CATEGORIES.length; i++) {
      let validCategory = VALID_CATEGORIES[i];
      for (let j = 0; j < CATEGORY_KEYWORDS[validCategory].length; j++) {
        const kw = CATEGORY_KEYWORDS[validCategory][j];
        if (category.toLowerCase().indexOf(kw) > -1) {
          return validCategory;
        }
      }
    }
    return null;
  };

  return Transaction;
};
