import {Request, Response} from "express";

const csvJson = require('csvjson');
const { getTransactions } = require("./footprintCalculator");
const { Transaction } = require("./models");

type APITransaction = {
    userId: number;
    memo: string;
    amount: number;
    date: Date;
    category1: string;
    category2: string;
}

const getAllTransactions = async (req: Request, res: Response) => {
    const transactionsObject = await getTransactions();
    res.json(transactionsObject);
};


const saveNewTransactions = async (req: Request, res: Response) => {
    if (req.body) {
        let transactionObject = [];
        try {
            transactionObject = csvJson.toObject(req.body);
            try {
                transactionObject.forEach(async (txnOrig: APITransaction) => {
                    const txn = {
                        userId: txnOrig.userId,
                        memo: txnOrig.memo,
                        transactionAmount: txnOrig.amount,
                        transactionDate: txnOrig.date,
                        carbonCategory: Transaction.tryFindCarbonCategory(txnOrig.category1)
                    };
                    if (!txn.carbonCategory) {
                        txn.carbonCategory = Transaction.tryFindCarbonCategory(txnOrig.category2);
                    }
                    await Transaction.create(txn);
                });
                res.sendStatus(202);
            } catch (e: any) {
                res.status(503);
                res.end('Failed inserting data into the database:\n' + e.toString());
            }
        } catch (e: any) {
            res.status(400);
            res.end('Unable to parse input csv:\n' + e.toString());
        }
    } else {
        res.status(400);
        res.end('empty POST body');
    }
};

module.exports = {getAllTransactions, saveNewTransactions}