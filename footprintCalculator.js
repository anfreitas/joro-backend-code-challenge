const { Transaction, User } = require("./models");
const carbonWeights = require("./config/carbonWeights")

const groupBy = (arr, key) => {
  return arr.reduce((acc, value) => {
    if (!acc[value[key]]) {
      acc[value[key]] = [];
    }
    acc[value[key]].push(value);
    return acc;
  }, {});
}

const getFootprint = async () => {
  const footprint = {};
  const transactions = await getTransactions();
  const transactionsByUser = groupBy(transactions.transactions, 'userId');
  const users = await User.findAll();
  Object.keys(transactionsByUser).forEach(userId => {
    footprint[userId] = {};
    const user = users.filter(u => u.id == userId)[0];
    const userTransactions = transactionsByUser[userId];
    const transactionsByDay = groupBy(userTransactions, 'transactionDate');
    Object.keys(transactionsByDay).forEach(date => {
      footprint[userId][date] = {};
      footprint[userId][date]['total'] = 0;
      const dateTransactions = userTransactions.filter(txn => txn.transactionDate.toString() === date.toString());
      Object.keys(carbonWeights).forEach(category => {
        const catTransactions = dateTransactions.filter(txn => txn.carbonCategory);
        if (catTransactions.length === 0) {
          footprint[userId][date][category] = 0;
        } else {
          let f = catTransactions.map(txn => txn.carbonAmount()).reduce((x, y) => x + y);
          if (category === 'food' && user.isVegetarian) {
            f *= 0.3
          }
          footprint[userId][date]['total'] += f
          footprint[userId][date][category] = f
        }
      })
    });
  })
  return {footprint: footprint};
}

const getTransactions = async () => {
  const transactions = await Transaction.findAll({order: [['transactionDate', 'ASC']]});
  return {transactions: transactions};
}

module.exports = {
  getFootprint,
  getTransactions
}
