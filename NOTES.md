## NOTES

- `yarn install` failed to complete with the latest `node` (currently `v19`). It may be helpful to include a `.node-version` file in the future.
- Used `csvjson` module to parse csv from endpoint
  - `csvtojson` is 8MB+ whereas `csvjson` is 36KB
- Readme states:
  > some of the transactions do not map to any category since they don't generate carbon emissions
  
  I did not find any transactions that generate no carbon emissions, however I can see that certain transactions might not fit exactly into any category (online/entertainment and financial/bank charge). These will likely create limited carbon emissions from computational work being done in data centers. For this I created a new carbonCategory `electronic_services` with a weight similar to that of `goods_and_services.
- I could foresee the `CATGEGORY_KEYWORDS` object growing massive and unwieldy over time, I'd maybe suggest pulling that out into its own component before that happens.