const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const app = require("../dist/app");
const models = require("../models");


describe("routes", function () {

  beforeEach(async function () {
    await models.User.create({
      firstName: "Test1",
      lastName: "Tesselate",
      email: "test@test.com"
    });
  });

  afterEach(async function () {
    await models.sequelize.truncate();
  })

  it("should return the Root route", function (done) {
    chai.request(app)
      .get('/')
      .end(function (err, res) {
        chai.expect(err).to.be.null;
        chai.expect(res).to.have.status(200);
        chai.expect(res.body.server_status).to.equal("working");
        done();
      });
  });

  it("should return a footprint object for the footprint route", function (done) {
    chai.request(app)
      .get('/footprint')
      .end(function (err, res) {
        chai.expect(err).to.be.null;
        chai.expect(res).to.have.status(200);
        chai.expect(res.body.footprint).to.be.an("object");
        done();
      });
  });

  it("should return a transactions array for the transactions route", function (done) {
    chai.request(app)
      .get('/transactions')
      .end(function (err, res) {
        chai.expect(err).to.be.null;
        chai.expect(res).to.have.status(200);
        chai.expect(res.body.transactions).to.be.an("array");
        done();
      });
  });

  it("should save a new transaction via the POST route and then return the newly saved transaction via the GET route", function (done) {
    const txn = 'userId,memo,amount,category1,category2,date\n1,Memo 1,45.92,restaurants,fast food,2020-03-03'
    chai.request(app)
      .post('/transactions')
      .set('content-type', 'text/csv')
      .send(txn)
      .end(function (postErr, postRes) {
        chai.expect(postErr).to.be.null;
        chai.expect(postRes).to.have.status(202);
        chai.request(app).get('/transactions').end(function (getErr, getRes) {
          chai.expect(getErr).to.be.null;
          chai.expect(getRes).to.have.status(200);
          chai.expect(getRes.body.transactions.length).to.equal(1);
          done();
        });
      })
  })

  it("should not save duplicate transactions", function (done) {
    const txn = 'userId,memo,amount,category1,category2,date\n1,Memo 1,45.92,restaurants,fast food,2020-03-03\n1,Memo 1,45.92,restaurants,fast food,2020-03-03'
    chai.request(app)
      .post('/transactions')
      .set('content-type', 'text/csv')
      .send(txn)
      .end(function (postErr, postRes) {
        chai.expect(postErr).to.be.null;
        chai.expect(postRes).to.have.status(202);
        chai.request(app).get('/transactions').end(function (getErr, getRes) {
          chai.expect(getErr).to.be.null;
          chai.expect(getRes).to.have.status(200);
          chai.expect(getRes.body.transactions.length).to.equal(1);
          done();
        });
      })
  })

});
