const { expect }  = require("chai");
const { getFootprint, getTransactions } = require("../footprintCalculator");
const models = require("../models");
const {Transaction} = require("sequelize");

describe("footprintCalculator", function () {

  describe("getTransactions", function () {

    let testUser;
    let testTransaction1;
    let testTransaction2;

    beforeEach(async function () {
      testUser = await models.User.create({
        firstName: "Test",
        lastName: "Tesselate",
        email: "test@test.com"
      });
      testTransaction1 = await models.Transaction.create({
        userId: testUser.id,
        memo: "Test transaction 1",
        carbonCategory: "food",
        transactionAmount: 105.10,
        transactionDate: "2020-03-01"
      });
      testTransaction2 = await models.Transaction.create({
        userId: testUser.id,
        memo: "Test transaction 2",
        carbonCategory: "home",
        transactionAmount: 254.22,
        transactionDate: "2020-03-02"
      });
    });

    afterEach(async function () {
      await models.sequelize.truncate();
    });

    it("should return an array of transactions", async function () {
      const transactionsObject = await getTransactions();
      const transactions = transactionsObject.transactions;
      const transactionUser = await transactions[0].getUser();
      expect(transactions.length).to.equal(2);
      expect(transactions[0].memo).to.equal(testTransaction1.memo);
      expect(transactionUser.id).to.equal(testUser.id);
    });

  });

  describe("getFootprint", function () {
    let testUser1;
    let testUser2;
    let testTransaction1;
    let testTransaction2;
    let testTransaction3;
    let testTransaction4;

    beforeEach(async function () {
      testUser1 = await models.User.create({
        firstName: "Test1",
        lastName: "Tesselate",
        email: "test@test.com"
      });
      testUser2 = await models.User.create({
        firstName: "Test2",
        lastName: "Tesselate",
        email: "test@test.com",
        isVegetarian: true
      });
      testTransaction1 = await models.Transaction.create({
        userId: testUser1.id,
        memo: "Test transaction 1",
        carbonCategory: "food",
        transactionAmount: 105.10,
        transactionDate: "2020-03-01"
      });
      testTransaction2 = await models.Transaction.create({
        userId: testUser2.id,
        memo: "Test transaction 2",
        carbonCategory: "food",
        transactionAmount: 254.22,
        transactionDate: "2020-03-02"
      });
      testTransaction3 = await models.Transaction.create({
        userId: testUser1.id,
        memo: "Test transaction 3",
        carbonCategory: "home",
        transactionAmount: 105.10,
        transactionDate: "2020-03-03"
      });
      testTransaction4 = await models.Transaction.create({
        userId: testUser2.id,
        memo: "Test transaction 4",
        carbonCategory: "home",
        transactionAmount: 254.22,
        transactionDate: "2020-03-03"
      });
    });

    afterEach(async function () {
      await models.sequelize.truncate();
    });

    it("should return a footprint object", async function () {
      const footprintObject = await getFootprint();
      const footprint = footprintObject.footprint;
      expect(Object.keys(footprint).length).to.equal(2);

      const userFootprint1 = footprint[testUser1.id];
      expect(Object.keys(userFootprint1).length).to.equal(2);
      expect(userFootprint1[new Date('2020-03-01')].food).to.equal(testTransaction1.carbonAmount())

      const userFootprint2 = footprint[testUser2.id];
      expect(Object.keys(userFootprint2).length).to.equal(2);
      expect(userFootprint2[new Date('2020-03-02')].food).to.equal(testTransaction2.carbonAmount() * 0.3)
    });

  })
})
